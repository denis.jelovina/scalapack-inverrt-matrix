# scalapack invert matrix

read matrix in MatrixMarket format and invert it 

compile with:

`mpif90 inv_martix.f90 -lscalapack`

run with

`mpirun -np 4 ./a.out rand_matrix.mtx`

the inverted matrix printed in fort.2000, fort.2001, fort.2002 ... fort.2003 (not very nice I know)
