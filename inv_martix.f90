program read_matrix_market
      use mpi
      implicit none


      integer :: nargs
      character(len=128) :: filename
      character(len=128) :: line
      integer :: procs
      integer :: mpirank
      integer :: ierr
      integer :: infile
      integer, dimension(MPI_STATUS_SIZE) :: mpistatus
      integer :: i,j,k
      double precision, dimension(10) :: areadaray
      double precision :: x
      integer :: n,m,isize,ibig,jbig
      integer :: nb,mb !block sizes 
      integer :: prow, pcol
      integer, dimension(2) :: pdims
      integer, allocatable, dimension(:) :: locrowlen,loccollen
      integer :: me, icontxt, myrow, mycol, myArows, myAcols  ! blacs data
      integer, external :: numroc,INDXG2L,INDXG2P,INDXL2G
      integer :: IIC,JJC,ICROW,ICCOL
      double precision, allocatable, dimension(:,:) :: locAmat,locAmat_inv
      integer, allocatable, dimension(:)   :: IPIV
      integer, dimension(9)   :: ides_a
      integer :: LWORK, LIWORK
      double precision, allocatable, dimension(:) :: WORK
      integer, allocatable, dimension(:) :: IWORK
      
      integer :: info    ! scalapack return value
      nb=64
      mb=64

! Initialize MPI (for MPI-IO)
      call MPI_Init(ierr)
      call MPI_Comm_size(MPI_COMM_WORLD,procs,ierr)
      call MPI_Comm_rank(MPI_COMM_WORLD,mpirank,ierr)

      write(1000+mpirank,*) "procs,mpirank=",procs,mpirank

! May as well get the process grid from MPI_Dims_create
      pdims = 0
      call MPI_Dims_create(procs, 2, pdims, ierr)
      prow = pdims(1)
      pcol = pdims(2)
      write(1000+mpirank,*) '              prow,pcol=',prow,pcol

! get filename
      nargs = command_argument_count()
      if (nargs /= 1) then
          write(1000+mpirank,*) 'Usage: darray filename'
          write(1000+mpirank,*) '       Where filename = name of binary matrix file.'
          call MPI_Abort(MPI_COMM_WORLD,1,ierr)
      endif
      call get_command_argument(1, filename)
      write(1000+mpirank,*) '              filename=',filename

! find the size of the array we'll be using


      open (unit = 128, file = filename, STATUS = "old")
      read (128,*) line
      read (128,*) n,m,isize
      write(1000+mpirank,*) '              n,m,isize=',n,m,isize

      call blacs_pinfo   (me,procs)
      call blacs_get     (-1, 0, icontxt)
      call blacs_gridinit(icontxt, 'R', prow, pcol)
      call blacs_gridinfo(icontxt, prow, pcol, myrow, mycol)

      
      myArows = numroc(n, nb, myrow, 0, prow)
      myAcols = numroc(m, mb, mycol, 0, pcol)

      allocate(locAmat(myArows,myAcols))
      locAmat=0.d0
      !allocate(locAmat_inv(myArows,myAcols))
      !locAmat_inv=0.d0
      
      write(1000+mpirank,*) '              myrow,mycol=',myrow,mycol
      write(1000+mpirank,*) '              myArows,myAcols=',myArows,myAcols

      do k=1,isize
         read (128,*) ibig,jbig,x
         
         IIC = INDXG2L( ibig, nb, 0, 0, prow)
         ICROW = INDXG2P (ibig, nb, 0, 0, prow)

         JJC = INDXG2L( jbig, mb, 0, 0, pcol)
         ICCOl = INDXG2P (jbig, mb, 0, 0, pcol)
         
         if(myrow==ICROW .and. mycol==ICCOL)then
            locAmat(IIC,JJC)=x
         end if

      end do
      close(128)

      call descinit( ides_a, n, m, nb, mb, 0, 0, icontxt, myArows, info )
      allocate(IPIV(min(m,n)))
      call pdgetrf(n, m , locAmat, 1, 1,ides_a, IPIV,INFO)
      write(1000+mpirank,*) '              INFO(pdgetrf)=',INFO

      allocate(WORK(1))
      allocate(IWORK(1))
      call pdgetri( n, locAmat, 1, 1, ides_a, IPIV, WORK, -1, IWORK, -1, INFO )
      LWORK=WORK(1)
      LIWORK=IWORK(1)
      deallocate(WORK)
      deallocate(IWORK)

      
      allocate(WORK(LWORK))
      allocate(IWORK(LIWORK))
      call pdgetri( n, locAmat, 1, 1, ides_a, IPIV, WORK, LWORK, IWORK, LIWORK, INFO )
      write(1000+mpirank,*) '              INFO(pdgetri)=',INFO



      do i=1,myArows
         do j=1,myAcols

            ibig=INDXL2G (i, nb, myrow, 0, prow)
            jbig=INDXL2G (j, mb, mycol, 0, pcol)
            if (locAmat(i,j) .ne. 0.d0) then
               write(2000+mpirank,*) ibig,jbig,locAmat(i,j)
            end if
         end do
      end do
         
      
      call MPI_Finalize(ierr)


    end program read_matrix_market

